﻿
#include <iostream>
#include <cmath>

class Vector
{
public:
    // Координаты вектора, сначала начальные, потом конечные
    Vector(int _x0, int _y0, int _z0, int _x, int _y, int _z) :x0(_x0), y0(_y0), z0(_z0), x(_x), y(_y), z(_z)
    {}
    void Show() // Вывод координат вектора
    {
        std::cout << '\n' << "Vector origin coordinates: " << "x0=" << x0 << ' ' << "y0=" << y0 << ' ' << "z0=" << z0;
        std::cout << '\n' << "Vector end coordinates: " << "x=" << x << ' ' << "y=" << y << ' ' << "z=" << z;
    }
    void Length() // Расчёт и вывод длины вектора
    {
        VectorLength = sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0));
        std::cout << '\n' << "Vector length = " << VectorLength;
    }
private:
    int x0, y0, z0; // Начальные координаты вектора
    int x, y, z; // Конечные координаты вектора
    float VectorLength; // Длина вектора
};



int main()
{
    std::cout << " Vector V1:";
    Vector V1(1, 4, 66, 34, 4, 2);
    V1.Show();
    V1.Length();
    std::cout << "\n \n Vector V2";

    Vector V2(44, 5, 43, 643, 23, 423);
    V2.Show();
    V2.Length();
    std::cout << "\n";

}